var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
mongoClient.connect('mongodb://usuario:senha@kamino.mongodb.umbler.com:49647/tutorial')
 //mongoClient.connect('mongodb://localhost:27017/workshop')
 .then(conn => global.conn = conn)
 .catch(err => console.log(err))
//module.exports = { }


function findAll(callback){
 global.conn.collection('customers').find({}).toArray(callback);
}
function insert(customer, callback){
 global.conn.collection('customers').insert(customer, callback);
}
function deleteOne(id, callback){
 global.conn.collection('customers').deleteOne({_id: new ObjectId(id)},
callback);
}

module.exports = { findAll, insert, deleteOne }
